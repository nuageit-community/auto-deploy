# Semantic Versioning Changelog

## [1.3.4](https://gitlab.com/nuageit-community/auto-deploy/compare/1.3.3...1.3.4) (2024-06-24)

### :bug: Fixes

* update to 1.25.2 ([e4dd8f9](https://gitlab.com/nuageit-community/auto-deploy/commit/e4dd8f93158bc382435653dd1008549cfb3a419f))

## [1.3.3](https://gitlab.com/nuageit-community/auto-deploy/compare/1.3.2...1.3.3) (2024-06-24)

### :memo: Docs

* change contributing ([3e614cc](https://gitlab.com/nuageit-community/auto-deploy/commit/3e614cc843e107e712f6686d06541a150bdfebf7))

### :bug: Fixes

* update auto deploy to 1.25.1 ([ebbe836](https://gitlab.com/nuageit-community/auto-deploy/commit/ebbe836a0fe7fcba171b5f424178c3898e9e1472))

## [1.3.2](https://gitlab.com/nuageit-community/auto-deploy/compare/1.3.1...1.3.2) (2024-06-22)

### :bug: Fixes

* organize script ([ca3b670](https://gitlab.com/nuageit-community/auto-deploy/commit/ca3b670b2ebe75a23d2d103d4e2da94b9e2d8cfc))
* organize script ([12cf8df](https://gitlab.com/nuageit-community/auto-deploy/commit/12cf8df9471c6efe42730f05150325028c27fa71))

## [1.3.1](https://gitlab.com/nuageit-community/auto-deploy/compare/1.3.0...1.3.1) (2024-03-22)


### :bug: Fixes

* community image ([64f5ed3](https://gitlab.com/nuageit-community/auto-deploy/commit/64f5ed3f8b17d20d0c0fd26b177a326fdf475033))

## [1.3.0](https://gitlab.com/nuageit-community/auto-deploy/compare/1.2.3...1.3.0) (2024-01-20)


### :memo: Docs

* auto deploy setup ([7c4e4be](https://gitlab.com/nuageit-community/auto-deploy/commit/7c4e4be0eb72601cec75828431ae6a8ad5a22e82))
* change script ([7fd1db7](https://gitlab.com/nuageit-community/auto-deploy/commit/7fd1db7837791a88e475ddc06b81547c771920e2))
* pretty readme ([06b6183](https://gitlab.com/nuageit-community/auto-deploy/commit/06b6183849ae9f5fe1cc17a3c0b5bf490ee92637))


### :repeat: CI

* change ref name ([3bb179a](https://gitlab.com/nuageit-community/auto-deploy/commit/3bb179af085a5616459c7d0c5facfbe958e5e694))
* include default tag ([e5837d0](https://gitlab.com/nuageit-community/auto-deploy/commit/e5837d010c0b6398550d662d29fc844533d4e96e))
* sobrescribe default ([f4d2964](https://gitlab.com/nuageit-community/auto-deploy/commit/f4d29644b172230f1ffba474164015eb76c987ab))


### :sparkles: News

* auto-deploy new image ([25497d6](https://gitlab.com/nuageit-community/auto-deploy/commit/25497d6542a8a79e073a4842acb2077de239ba95))


### :bug: Fixes

* change auto deploy code ([8d63eda](https://gitlab.com/nuageit-community/auto-deploy/commit/8d63edafdeefd7701e42493712dfe8234018ad7b))
* change auto deploy version ([d9c305b](https://gitlab.com/nuageit-community/auto-deploy/commit/d9c305b4d2bb8215970020a163ba09b43b7574d6))
* change pipeline template ([98f96cb](https://gitlab.com/nuageit-community/auto-deploy/commit/98f96cb0a509fe320d3f16c693bd1dd17090f56d))
* configs files ([fea296b](https://gitlab.com/nuageit-community/auto-deploy/commit/fea296ba78932bc260d0352e7960a1ddf8aaad0c))
* identation yaml ([ea5a987](https://gitlab.com/nuageit-community/auto-deploy/commit/ea5a9872f1e87df893ddf3a645547148169e93b0))
* organize project setup ([d2f9bd6](https://gitlab.com/nuageit-community/auto-deploy/commit/d2f9bd61dd9238562fcbd58eaba41fb00ca2e450))
* setup docs and taskfile ([7644ca0](https://gitlab.com/nuageit-community/auto-deploy/commit/7644ca0080264aaf803e45488144c0e2ae595acc))
* update version ([b7e1e20](https://gitlab.com/nuageit-community/auto-deploy/commit/b7e1e20826d43eec42d89559f5755066e1ad3912))

## [1.3.1](https://gitlab.com/nuageit-community/auto-deploy/compare/1.3.0...1.3.1) (2023-11-01)


### :bug: Fixes

* change auto deploy version ([d9c305b](https://gitlab.com/nuageit-community/auto-deploy/commit/d9c305b4d2bb8215970020a163ba09b43b7574d6))

## [1.3.0](https://gitlab.com/nuageit-community/auto-deploy/compare/1.2.3...1.3.0) (2023-10-31)


### :memo: Docs

* auto deploy setup ([7c4e4be](https://gitlab.com/nuageit-community/auto-deploy/commit/7c4e4be0eb72601cec75828431ae6a8ad5a22e82))
* change script ([7fd1db7](https://gitlab.com/nuageit-community/auto-deploy/commit/7fd1db7837791a88e475ddc06b81547c771920e2))
* pretty readme ([06b6183](https://gitlab.com/nuageit-community/auto-deploy/commit/06b6183849ae9f5fe1cc17a3c0b5bf490ee92637))


### :repeat: CI

* change ref name ([3bb179a](https://gitlab.com/nuageit-community/auto-deploy/commit/3bb179af085a5616459c7d0c5facfbe958e5e694))
* include default tag ([e5837d0](https://gitlab.com/nuageit-community/auto-deploy/commit/e5837d010c0b6398550d662d29fc844533d4e96e))
* sobrescribe default ([f4d2964](https://gitlab.com/nuageit-community/auto-deploy/commit/f4d29644b172230f1ffba474164015eb76c987ab))


### :bug: Fixes

* change auto deploy code ([8d63eda](https://gitlab.com/nuageit-community/auto-deploy/commit/8d63edafdeefd7701e42493712dfe8234018ad7b))
* change pipeline template ([98f96cb](https://gitlab.com/nuageit-community/auto-deploy/commit/98f96cb0a509fe320d3f16c693bd1dd17090f56d))
* configs files ([fea296b](https://gitlab.com/nuageit-community/auto-deploy/commit/fea296ba78932bc260d0352e7960a1ddf8aaad0c))
* identation yaml ([ea5a987](https://gitlab.com/nuageit-community/auto-deploy/commit/ea5a9872f1e87df893ddf3a645547148169e93b0))
* organize project setup ([d2f9bd6](https://gitlab.com/nuageit-community/auto-deploy/commit/d2f9bd61dd9238562fcbd58eaba41fb00ca2e450))
* setup docs and taskfile ([7644ca0](https://gitlab.com/nuageit-community/auto-deploy/commit/7644ca0080264aaf803e45488144c0e2ae595acc))


### :sparkles: News

* auto-deploy new image ([25497d6](https://gitlab.com/nuageit-community/auto-deploy/commit/25497d6542a8a79e073a4842acb2077de239ba95))

## [1.2.3](https://gitlab.com/nuageit-community/auto-deploy/compare/1.2.2...1.2.3) (2023-03-17)


### :bug: Fixes

* version to 1.21.4 ([c8ba3c3](https://gitlab.com/nuageit-community/auto-deploy/commit/c8ba3c3c208c1edd3554a78ba3e35fdbc092865d))

## [1.2.2](https://gitlab.com/nuageit-community/auto-deploy/compare/1.2.1...1.2.2) (2023-03-17)


### :bug: Fixes

* update image version ([5450ad0](https://gitlab.com/nuageit-community/auto-deploy/commit/5450ad0387fa7f80471c1b592899774069aa3980))

## [1.2.1](https://gitlab.com/nuageit-community/auto-deploy/compare/1.2.0...1.2.1) (2023-03-17)


### :bug: Fixes

* update 1.21.1 ([5c1d0ca](https://gitlab.com/nuageit-community/auto-deploy/commit/5c1d0cabc6ce22edd043250da05bd08f8d93bb8a))

## [1.2.0](https://gitlab.com/nuageit-community/auto-deploy/compare/1.1.7...1.2.0) (2023-03-17)


### :zap: Refactoring

* update file name ([b5b2b3b](https://gitlab.com/nuageit-community/auto-deploy/commit/b5b2b3baf8391d87e7b62722ed8f7129c830af09))


### :bug: Fixes

* **SC-425:** update var to 1.21.0 ([90eb501](https://gitlab.com/nuageit-community/auto-deploy/commit/90eb501c9f93de9183386dce074bce280402595c))

## [1.1.7](https://gitlab.com/nuageit-community/auto-deploy/compare/1.1.6...1.1.7) (2023-03-16)


### :bug: Fixes

* **JDS-81:** update community project ([5e208be](https://gitlab.com/nuageit-community/auto-deploy/commit/5e208bef155c7c55754a1f47e0c7c42c97f2bfe3))


### :repeat: CI

* change include ([ac8242d](https://gitlab.com/nuageit-community/auto-deploy/commit/ac8242dfc31770137c2ce26bb6b2b13024711840))

## [1.1.6](https://gitlab.com/nuageit-community/auto-deploy/compare/1.1.5...1.1.6) (2023-03-02)


### :bug: Fixes

* change auto deploy version to 1.19.3 ([8f021b7](https://gitlab.com/nuageit-community/auto-deploy/commit/8f021b703cc7ab21808311694b80546c5835b3c0))

## [1.1.5](https://gitlab.com/nuageit-community/auto-deploy/compare/1.1.4...1.1.5) (2023-03-01)


### :bug: Fixes

* update to 1.19.2 ([798defb](https://gitlab.com/nuageit-community/auto-deploy/commit/798defb867557460a79e3a670a72c98e0a4206a4))

## [1.1.4](https://gitlab.com/nuageit-community/auto-deploy/compare/1.1.3...1.1.4) (2023-03-01)


### :bug: Fixes

* change tag to 1.19.1 ([9f381a5](https://gitlab.com/nuageit-community/auto-deploy/commit/9f381a590f9403de719bf75772bcadfc49b1507d))

## [1.1.3](https://gitlab.com/nuageit-community/auto-deploy/compare/1.1.2...1.1.3) (2023-03-01)


### :bug: Fixes

* **JDS-47:** change image version arg ([b89ad77](https://gitlab.com/nuageit-community/auto-deploy/commit/b89ad7734ebe577d4a32e008fb45482bcc4052d1))


### :repeat: CI

* **JDS-47:** include projects ([d7474ce](https://gitlab.com/nuageit-community/auto-deploy/commit/d7474ce8c1e57a8f515f5ea0b7e1f39e42ca8075))

## [1.1.2](https://gitlab.com/nuageit-community/auto-deploy/compare/1.1.1...1.1.2) (2022-12-30)


### :bug: Fixes

* use 1.18.0 version ([a9b3c7f](https://gitlab.com/nuageit-community/auto-deploy/commit/a9b3c7fa68808c13cf6d7370d718ecae9b17c698))

## [1.1.1](https://gitlab.com/nuageit-community/auto-deploy/compare/1.1.0...1.1.1) (2022-12-30)


### :bug: Fixes

* target image auto deploy priv ([b211ab7](https://gitlab.com/nuageit-community/auto-deploy/commit/b211ab77d9fbb2d5db19b15b308d85a5ed1b5c7e))

## [1.1.0](https://gitlab.com/nuageit-community/auto-deploy/compare/1.0.0...1.1.0) (2022-12-30)


### :repeat: CI

* add variables section ([caa17b8](https://gitlab.com/nuageit-community/auto-deploy/commit/caa17b8aec7e10bc3a81cab2ef54e89b93fa2eb3))


### :sparkles: News

* add arg AUTO_DEPLOY_VERSION ([a9b4dcc](https://gitlab.com/nuageit-community/auto-deploy/commit/a9b4dcc0724d16eae8d6d8b472540401cf072c7f))
* update image auto deploy ([9f1d3d0](https://gitlab.com/nuageit-community/auto-deploy/commit/9f1d3d05adb627875f40a46dc79d5e0e55d88778))

## 1.0.0 (2022-12-30)


### :sparkles: News

* add config files ([ca1f1fb](https://gitlab.com/nuageit-community/auto-deploy/commit/ca1f1fba58560bf703196efa085c8c0607da0431))
* initial commit ([68b9ea2](https://gitlab.com/nuageit-community/auto-deploy/commit/68b9ea23eadcf95ab9f9bcda6767a8989a25a0a1))


### :repeat: CI

* add semantic release ([4cb0d8e](https://gitlab.com/nuageit-community/auto-deploy/commit/4cb0d8e2dc72bd74aeab19ec7802777e1b2adba6))
* add workflow and stages ([2839d63](https://gitlab.com/nuageit-community/auto-deploy/commit/2839d636f6c2c82fc34ab306d7ca0ffb7ea057f8))
* change includes ([2f0c392](https://gitlab.com/nuageit-community/auto-deploy/commit/2f0c392d3c9fb4a78ffd33a05237111a5774dfb3))
